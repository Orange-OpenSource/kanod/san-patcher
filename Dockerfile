#!/bin/bash

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

FROM python:3.9-slim
LABEL project=kanod-cert-patcher
ENV IRONIC_GID=994
ENV IRONIC_UID=997

RUN mkdir /install
RUN pip install kubernetes cryptography netifaces
RUN groupadd -r ironic -g "${IRONIC_GID}"
RUN useradd -r -g ironic -u "${IRONIC_UID}" -s /sbin/nologin ironic -d /var/lib/ironic
COPY main.py /install
USER ${IRONIC_UID}
ENTRYPOINT ["python3", "/install/main.py"]
