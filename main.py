#!/bin/bash

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import os
import netifaces
import time

from cryptography import x509
from cryptography.hazmat.backends import default_backend

from kubernetes import client  # type: ignore
from kubernetes import config  # type: ignore


def get_ips_from_cert():
    try:
        with open('/mnt/tls.crt', 'r') as fd:
            data = fd.read().encode()
            cert = x509.load_pem_x509_certificate(data, default_backend())
            san = cert.extensions.get_extension_for_oid(
                x509.extensions.SubjectAlternativeName.oid).value
            return [str(ip) for ip in san.get_values_for_type(x509.IPAddress)]
    except Exception:
        return []


# Why is there no default function in ?
def get_node(api_core, node_name):
    """get a kubernetes node object having a given name."""
    nodes = api_core.list_node()
    return next((n for n in nodes.items if n.metadata.name == node_name), None)


def main():
    print('Loading config')
    config.load_incluster_config()
    api_custom = client.CustomObjectsApi()
    api_core = client.CoreV1Api()

    name = os.environ['NAME']
    namespace = os.environ.get('NAMESPACE', 'default')
    ip_address = os.environ.get('PROVISIONING_IP', None)
    if ip_address is None:
        provisioning = os.environ.get('PROVISIONING_INTERFACE', None)
        if provisioning is None:
            node_name = os.environ.get('NODE_NAME', None)
            if node_name is None:
                print('no PROVISIONING_INTERFACE and no NODE_NAME')
                exit(1)
            node = get_node(api_core, node_name)
            if node is None:
                print('wrong node')
                exit(1)
            provisioning = node.metadata.labels.get(
                'kanod.io/ironic_interface')
            if provisioning is None:
                print('did not find the annotation for provisioning interface')
                exit(1)
            print(f'using provisioning interface from node label : {provisioning}')
        else:
            print(f'using provisioning interface from env : {provisioning}')

        cm_name = os.environ.get('IRONIC_CM_NAME', None)
        cm_ns = os.environ.get('IRONIC_CM_NAMESPACE', None)
        if cm_name is not None and cm_ns is not None:
            api_core.patch_namespaced_config_map(
                name=cm_name, namespace=cm_ns,
                body={
                    'data': {
                        'PROVISIONING_INTERFACE': provisioning
                    }
                }
            )
        else:
             print('no configmap specified for ironic config')

        ni = netifaces.ifaddresses(provisioning)
        ip_address = ni[netifaces.AF_INET][0]['addr']
        # command = [
        #    'ip', '-br', 'add', 'show', 'scope', 'global', 'up', 'dev',
        #    provisioning]
        # p = subprocess.Popen(command, stdout=subprocess.PIPE)
        # result = p.communicate()[0]
        # ip_address = result.decode().split()[2].split('/')[0]
    else:
        print(f'using provisioning ip from env : {ip_address}')

    print('Patching for', ip_address)
    api_custom.patch_namespaced_custom_object(
        group='cert-manager.io',
        version='v1',
        namespace=namespace,
        name=name,
        plural='certificates',
        body={
            'spec': {
                'ipAddresses': [ip_address]
            }
        }
    )

    print('Patching done')
    ips = []
    while ip_address not in ips:
        ips = get_ips_from_cert()
        print('found ' + ' '.join(ips))
        time.sleep(5)
    print('Reached')


main()
